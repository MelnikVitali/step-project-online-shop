Technology used:
---------------
- GULP 4
- Bootstrap 4
- Font Awesome 5
- SCSS
- jquery

Project participants:
---------------------
1. Student 1 - Bodnar Yurii
Tasks(sections):
- navbar
- header

2. Student 2 - Melnik Vitalii
Tasks(sections):
- stocks
- furniture
- filter
- blog
- newsletter
- footer
- copyright
- modal(modal window)


To run a project:
-----------------
- npm install
- gulp dev
- gulp build
- gulp clean

Quick delete folder node_modules:
---------------------------------
- npm install rimraf -g
- rimraf node_modules










